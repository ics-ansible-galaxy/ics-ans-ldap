import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ldap')


def test_slapd(host):
    service = host.service("slapd")
    assert service.is_running
    assert service.is_enabled


def test_lsc(host):
    service = host.service("lsc-unit")
    assert not service.is_running
    assert not service.is_enabled


def test_ldap_confidentiality(host):
    cmd = host.run("ldapsearch")
    assert cmd.rc == 13
    assert "Confidentiality required" in cmd.stderr


def test_ldap_namingcontext(host):
    cmd = host.run("ldapsearch -x -Z -s base namingContexts")
    assert cmd.rc == 0
    assert "namingContexts: dc=esss,dc=lu,dc=se" in cmd.stdout
    assert "numEntries: 1" in cmd.stdout


def test_ldap_search(host):
    cmd = host.run("ldapsearch -Z -x -s sub -b dc=esss,dc=lu,dc=se uid=testuser")
    assert cmd.rc == 0
    assert "numEntries: 1" in cmd.stdout


def test_ldap_bind(host):
    cmd = host.run("ldapsearch -Z -D cn=Manager,dc=esss,dc=lu,dc=se -w changemenow -s sub -b dc=esss,dc=lu,dc=se uid=testuser")
    assert cmd.rc == 0
    assert "numEntries: 1" in cmd.stdout
